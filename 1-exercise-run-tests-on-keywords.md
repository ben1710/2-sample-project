# Exercise 1

We will start getting serious. Our demo project will deal with the comfortable winter here in Helsinki, where nights are clear and Santa Clause lives near by. We will utilize a Finnish website providing weather data. While we would prefer using APIs, we are going to use web ui for educational reasons only.

In this exercise we will use:
1. GitLab Pipeline
1. Merge Requests
1. Docker Images with set up SeleniumLibrary and browsers

## 1: Inspect branch

* [ ] **Take a look around:**

- `resources` folder contains a resource file with keywords used in a later exercise
- `tests` folder contains a robot test suite running tests on keywords of the resource file we just had looked at
- `.gitlab-ci.yml` contains a disabled job *Test keywords*

## 2: Activate disabled job
* [ ] **In your pipeline, activate the job *Test keywords***

In gitlab-ci, every job starting with a point is disabled. By simply removing the dot, a job automatically becomes activated.

:bulb: Jobs can inherit other jobs. It is common practice to use disabled jobs as *parent job* or *blueprint* providing most common configurations. For further information check out documentation about [*extends* at gitlab](https://docs.gitlab.com/ee/ci/yaml/README.html#extends)

## 3: Create Merge Request on to develop
* [ ] **Create a merge request for this branch on to develop**

Although our pipeline breaks, we already can open a merge request. Until we merge, no harm is done and we can keep this merge request open until it is fixed.

:bulb: Every merge request that does not have any merge conflicts offers option to *Merge when pipeline succeeds*. That way, the merge requests closes automatically once its pipeline succeeds. This saves time, as for long running pipelines, you can continue with further tasks. Like getting :coffee:

## 4: Fix pipeline
* [ ] **Make tests in this branch pass and use previously opened merge request for merging changes on to develop**

Check error messages in your pipeline and fix it. We want all tests for our keywords to pass and merge changes on to develop.

## 5: Advanced exercises

### Limit execution of tests to certain changes
* [ ] **Think of changes where you do not want to run tests and limit test execution only to changes you actually need tests**

There can never be enough testing - unless time and money are running out. Tests consume resources, block queues where pipeline pile up waiting for execution. Maybe you can exclude a few files or even folders from triggering test execution. When you come up with an idea, check out documentation about [*only* and *except* sections for gitlab pipelines](https://docs.gitlab.com/ee/ci/yaml/README.html#onlychangesexceptchanges).

### Create a parent job for jobs requiring webdrivers, browsers and Seleniumlibrary
* [ ] **Create a disabled job containing everything you need for executing selenium library tests**
* [ ] **Make test job extend this new parent job**

[*extends* at gitlab](https://docs.gitlab.com/ee/ci/yaml/README.html#extends)

### Extract parent job for selenium tests to own yaml file
* [ ] **Extract parent job to own yaml file**
* [ ] **Make test job include and extend extracted parent job**

[*include* other yaml files in gitlab](https://docs.gitlab.com/ee/ci/yaml/README.html#includelocal)
